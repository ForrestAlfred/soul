//
//  SoulCore.m
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import "SoulCore.h"
#import "SoulBlocks.h"
#import "KVObserver.h"
@implementation Soul
+ (void)observe:(id)object keyPath:(NSString *)keyPath block:(SoulObjectBlock)soulObjectBlock {
    [PMKVObserver observe:self keyPath:keyPath block:soulObjectBlock];
}
@end
