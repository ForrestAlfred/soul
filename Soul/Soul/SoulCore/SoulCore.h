//
//  SoulCore.h
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoulBlocks.h"

// categories of APLClasses do not need to imported
//#import "NSObject+KVObseve.h"

@interface Soul : NSObject
+ (void)observe:(id)object keyPath:(NSString *)keyPath block:(SoulObjectBlock)soulObjectBlock;

@end
