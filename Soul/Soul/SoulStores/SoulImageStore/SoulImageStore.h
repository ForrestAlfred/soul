//
//  ImageStore.h
//  LeedsHouse
//
//  Created by yons on 15/8/26.
//  Copyright (c) 2015年 Alfred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SoulImageStore : NSObject

+ (instancetype)sharedStore;

- (void)setImage:(UIImage *)image forKey:(NSString *)key;

- (UIImage *)imageForKey:(NSString *)key;

- (void)deleteImageForKey:(NSString *)key;

- (NSString *)imagePathForKey:(NSString *)key;

- (void)setSnapshot:(UIView *)view forKey:(NSString *)key;

@end
