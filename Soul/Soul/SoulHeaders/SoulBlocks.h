//
//  SoulBlocks.h
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SoulSize.h"

typedef void (^SoulBlock)();
typedef void (^SoulParamsBlock)(NSDictionary *params);
typedef void (^SoulCellBlock)(UITableViewCell *cell);
typedef void (^SoulInitBlock)(NSDictionary *params);

typedef void (^SoulObjectBlock)(id object);
typedef void (^SoulObjectChangeBlock)(id newObject);
    // forOld
// forOld
typedef void (^ActionBlock)();
typedef void (^VissueBlock)();
typedef void (^VissueParamBlock)(NSDictionary *param);

typedef void (^ViewBlock)();

//SoulMacro
#define pass nil;
