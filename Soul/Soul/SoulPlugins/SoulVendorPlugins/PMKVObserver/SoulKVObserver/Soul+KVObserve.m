//
//  Soul+KVObserve.m
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import "Soul+KVObserve.h"
#import "KVObserver.h"
#import <ReactiveCocoa.h>
#import <LinkBlock.h>

@implementation Soul (KVObserve)
+ (void)assignCoreInitator:(id)initator keyPath:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector  {
    if (!self) {
            // throw error
            // should report initator is nil;
    }
    NSString *firstCharacter = [[NSStringFromSelector(targetSelector) substringToIndex:1] uppercaseString];
    NSString *restCharacters = [NSStringFromSelector(targetSelector) substringFromIndex:1];
    __block NSString *targetSetterKeyPath = @"set".strAppend(firstCharacter).strAppend(restCharacters).strAppend(@":");
    
    SEL targetSetterSelector = NSSelectorFromString(targetSetterKeyPath);
    
    if ([initator respondsToSelector:keyPathSelector]) {
        if ([target respondsToSelector:targetSelector]) {
            if ([target respondsToSelector:targetSetterSelector]) {
                
                IMP initatorImp = [initator methodForSelector:keyPathSelector];
                id (*initatorFunc)(id, SEL) = (void *)initatorImp;
                id getterInitatorProperty = initatorFunc(initator, keyPathSelector);
                if (getterInitatorProperty) {
                    IMP targetImp = [initator methodForSelector:targetSetterSelector];
                    void (*targetFunc)(id, SEL, id targetProperty) = (void *)targetImp;
                    targetFunc(target, targetSetterSelector, getterInitatorProperty);
                }
            }
        }
    }
}

+ (void)assignSoulInitator:(id)initator keyPath:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector  {
    [self assignCoreInitator:initator keyPath:keyPathSelector toTarget:target keyPath:targetSelector];
}


+ (void)syncSoulInitator:(id)initator keyPath:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector  {

    [self assignCoreInitator:initator keyPath:keyPathSelector toTarget:target keyPath:targetSelector];
    
    __block NSString *keyPath = NSStringFromSelector(keyPathSelector);
    [PMKVObserver observeObject:initator keyPath:keyPath
                        options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                          block:^(id  _Nonnull object, NSDictionary<NSString *,id> * _Nullable change, PMKVObserver * _Nonnull kvo) {
                              [self assignCoreInitator:initator keyPath:keyPathSelector toTarget:target keyPath:targetSelector];
                          }];
}
@end
