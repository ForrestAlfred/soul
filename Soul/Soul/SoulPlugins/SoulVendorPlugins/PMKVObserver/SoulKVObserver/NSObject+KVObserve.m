//
//  NSObject+KVObserve.m
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import "NSObject+KVObserve.h"
#import "KVObserver.h"
#import "Soul+KVObserve.h"
#import "LxDBAnything.h"
@implementation NSObject (KVObserve)

- (void)observe:(SEL)keyPathSelector next:(SoulObjectBlock)soulObjectBlock {
    NSString* keyPathString = NSStringFromSelector(keyPathSelector);
    if (!self) {
//        NSString *reason = [NSString stringWithFormat:@"Expected a @property (nonatomic,strong)NSString* xmlNodeName; property on %@",model];
//        @throw [NSException exceptionWithName:@"ADModelToStingParser xmlNodeName property Warning"
//                                       reason:reason
//                                     userInfo:nil];
    }
    [PMKVObserver observe:self keyPath:keyPathString block:soulObjectBlock];
}
- (void)observeChange:(SEL)keyPathSelector next:(SoulObjectChangeBlock)soulObjectBlock {
    NSString* keyPathString = NSStringFromSelector(keyPathSelector);
    if (!self) {
            //        NSString *reason = [NSString stringWithFormat:@"Expected a @property (nonatomic,strong)NSString* xmlNodeName; property on %@",model];
            //        @throw [NSException exceptionWithName:@"ADModelToStingParser xmlNodeName property Warning"
            //                                       reason:reason
            //                                     userInfo:nil];
    }
    [PMKVObserver observeChange:self keyPath:keyPathString block:soulObjectBlock];
}

- (void)assignCore:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector {
    [Soul assignSoulInitator:self keyPath:keyPathSelector toTarget:target keyPath:targetSelector];
}
// generate from core
- (void)assign:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector {
    [self assignCore:keyPathSelector toTarget:target keyPath:targetSelector];
}
// generate from converting
- (void)assign:(SEL)keyPathSelector fromInitator:(id)initator keyPath:(SEL)initatorSelector {
    [initator assignCore:initatorSelector toTarget:self keyPath:keyPathSelector];
}
// convenience
- (void)assign:(SEL)keyPathSelector to:(id)target at:(SEL)targetSelector {
    [self assign:keyPathSelector toTarget:target keyPath:targetSelector];
}
- (void)assign:(SEL)keyPathSelector from:(id)initator at:(SEL)initatorSelector {
    [initator assign:initatorSelector toTarget:self keyPath:keyPathSelector];
}
    // updating more convenience
    // for performance using origional method
- (void)assign:(SEL)keyPathSelector toTarget:(id)target {
    [Soul assignSoulInitator:self keyPath:keyPathSelector toTarget:target keyPath:keyPathSelector];
}
- (void)assign:(SEL)keyPathSelector fromInitator:(id)initator {
    [Soul assignSoulInitator:initator keyPath:keyPathSelector toTarget:self keyPath:keyPathSelector];
}
    // updating convenience
- (void)assign:(SEL)keyPathSelector to:(id)target {
    [self assign:keyPathSelector toTarget:target];
}
- (void)assign:(SEL)keyPathSelector from:(id)initator {
    [self assign:keyPathSelector fromInitator:initator];
}


- (void)syncCore:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector {
    [Soul syncSoulInitator:self keyPath:keyPathSelector toTarget:target keyPath:targetSelector];
}
// generate from core
- (void)sync:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector {
    [self syncCore:keyPathSelector toTarget:target keyPath:targetSelector];
}
// generate from converting
- (void)sync:(SEL)keyPathSelector fromInitator:(id)initator keyPath:(SEL)initatorSelector {
    [initator syncCore:initatorSelector toTarget:self keyPath:keyPathSelector];
}

// convenience
- (void)sync:(SEL)keyPathSelector to:(id)target at:(SEL)targetSelector {
    [self sync:keyPathSelector toTarget:target keyPath:targetSelector];
}
- (void)sync:(SEL)keyPathSelector from:(id)initator at:(SEL)initatorSelector {
    [initator sync:initatorSelector toTarget:self keyPath:keyPathSelector];
}

// more convenience
- (void)sync:(SEL)keyPathSelector toTarget:(id)target {
    [Soul syncSoulInitator:self keyPath:keyPathSelector toTarget:target keyPath:keyPathSelector];
}
- (void)sync:(SEL)keyPathSelector fromInitator:(id)initator {
    [Soul syncSoulInitator:initator keyPath:keyPathSelector toTarget:self keyPath:keyPathSelector];
}

// much more convenience
- (void)sync:(SEL)keyPathSelector to:(id)target {
    [self sync:keyPathSelector toTarget:target];
}
- (void)sync:(SEL)keyPathSelector from:(id)initator {
    [self sync:keyPathSelector fromInitator:initator];
}
@end
