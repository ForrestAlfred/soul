//
//  NSObject+KVObserve.h
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoulBlocks.h"

@interface NSObject (KVObserve)
//- (void)observeSoul:(NSString *)keyPath block:(SoulObjectBlock)soulObjectBlock;

//- (void)sync:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector;

//- (void)sync:(SEL)keyPathSelector toTarget:(id)target;
//- (void)sync:(SEL)keyPathSelector fromInitator:(id)initator;
- (void)observe:(SEL)keyPathSelector next:(SoulObjectBlock)soulObjectBlock;
- (void)observeChange:(SEL)keyPathSelector next:(SoulObjectBlock)soulObjectBlock;

- (void)assign:(SEL)keyPathSelector from:(id)initator;
- (void)assign:(SEL)keyPathSelector to:(id)target;
- (void)assign:(SEL)keyPathSelector from:(id)initator at:(SEL)initatorSelector;
- (void)assign:(SEL)keyPathSelector to:(id)target at:(SEL)targetSelector;

- (void)sync:(SEL)keyPathSelector from:(id)initator;
- (void)sync:(SEL)keyPathSelector to:(id)target;
- (void)sync:(SEL)keyPathSelector from:(id)initator at:(SEL)initatorSelector;
- (void)sync:(SEL)keyPathSelector to:(id)target at:(SEL)targetSelector;


// TODO:
- (void)sync:(SEL)keyPathSelector from:(id)initator next:(SoulObjectBlock)objectBlock;
- (void)sync:(SEL)keyPathSelector to:(id)target next:(SoulObjectBlock)objectBlock;
- (void)sync:(SEL)keyPathSelector from:(id)initator at:(SEL)initatorSelector next:(SoulObjectBlock)objectBlock;
- (void)sync:(SEL)keyPathSelector to:(id)target at:(SEL)targetSelector next:(SoulObjectBlock)objectBlock;



@end
