//
//  Soul+KVObserve.h
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import "SoulCore.h"

@interface Soul (KVObserve)
+ (void)syncSoulInitator:(id)initator keyPath:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector;
+ (void)assignSoulInitator:(id)initator keyPath:(SEL)keyPathSelector toTarget:(id)target keyPath:(SEL)targetSelector;
@end
