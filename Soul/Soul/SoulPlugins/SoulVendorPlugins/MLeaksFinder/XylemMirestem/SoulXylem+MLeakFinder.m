//
//  SoulXylem+MLeakFinder.m
//  Soul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulXylem+MLeakFinder.h"
#import "MLeaksFinder.h"

@implementation SoulXylem (MLeakFinder)
- (BOOL)willDealloc {
    if (![super willDealloc]) {
        return NO;
    }
    
    MLCheck(self.scope);
    MLCheck(self.scope.missue);
    MLCheck(self.scope.view);
    
    return YES;
}
@end
