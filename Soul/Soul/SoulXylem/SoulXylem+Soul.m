//
//  SoulXylem+Soul.m
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/2/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulXylem+Soul.h"

#import <ReactiveCocoa.h>

@implementation SoulXylem (Soul)
- (void)init:(SoulInitBlock)soulInitBlock {
    [self.soul subscribeNext:soulInitBlock];
}
@end
