//
//  SoulXylem+XylemResolver.h
//  Soul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulXylem.h"

@interface SoulXylem (XylemResolver)
- (void)soulBlockResolve:(SoulBlock)soulBlock;
- (id)initXylemWithParams:(NSDictionary *)params;

@end
