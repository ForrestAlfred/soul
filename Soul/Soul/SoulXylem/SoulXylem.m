    //
    //  SoulXylem.m
    //  EvolutionForSoul
    //
    //  Created by 徐腾浩 on 4/1/16.
    //  Copyright © 2016 daopr. All rights reserved.
    //

#import "SoulXylem.h"
#import "SoulXylem+XylemResolver.h"
#import <Masonry.h>


@interface SoulXylem ()

@end

@implementation SoulXylem

    // Example
- (void)initSoul {
//    [self init:^(NSDictionary *params) {
//            //        self.scopeClass = [xxxScope class];
//    }];
}

- (void)soulInit {
    
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Soul Warning"
                                   reason:@"SoulXylem Init ItSelf"
                                 userInfo:nil];
    
}

- (id)initWithRouterParams:(NSDictionary *)params {
    return [self initXylemWithParams:params];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.view addSubview:self.background];
    [self.background mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];

    [self.scope soulWillAppearMethod];
    
//
    [self.background addSubview:self.scope.view];
    [self.scope.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.background);
    }];

//    soulBackground.backgroundColor = [UIColor yellowColor];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self soulBlockResolve:self.soulWillDisappear];
//    [self soulBlockResolve:self.scope.soulWillDisappear];
    [self.scope soulWillDisappearMethod];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self soulBlockResolve:self.soulDidLoad];
    
    [self soulBlockResolve:self.scope.soulDidLoad];
    [self.scope soulDidLoadMethod];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self soulBlockResolve:self.soulMemoryWarning];
    
    [self soulBlockResolve:self.scope.soulMemoryWarning];
    [self.scope soulMemoryWarningMethod];
}

- (void)dealloc {
    [self soulBlockResolve:self.soulDealloc];
}

@end
