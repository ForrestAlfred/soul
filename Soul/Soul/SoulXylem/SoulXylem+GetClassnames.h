//
//  SoulXylem+GetClassnames.h
//  Soul
//
//  Created by 徐腾浩 on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulXylem.h"

@interface SoulXylem (GetClassnames)

- (Class)getDefaultScopeClass;
@end
