//
//  SoulXylem+XylemResolver.m
//  Soul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulXylem+XylemResolver.h"
#import "SoulXylem+GetClassnames.h"
#import <ReactiveCocoa.h>

@implementation SoulXylem (XylemResolver)
- (void)soulBlockResolve:(SoulBlock)soulBlock {
    soulBlock?soulBlock():nil;
}

- (id)initXylemWithParams:(NSDictionary *)params {
    if (!(self = [super init]))   {return nil;}
    
    @weakify(params);
    self.soul = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(params);
        [subscriber sendNext:params];
        [subscriber sendCompleted];
        return nil;
    }];
    
    [self initSoul];
    [self soulInit];
    
    self.scope = [self scopeTypeResolverWithParams:params];
    
    self.background = [UIView new];
    self.background.backgroundColor = [UIColor purpleColor];
    self.scope.background = self.background;
    self.scope.view.background = self.background;
    
    [self soulInit];
     // for did load
    
    return self;
}


- (id)scopeTypeResolverWithParams:(NSDictionary *)params {
    if (!self.scopeClass) {
      self.scopeClass = [self getDefaultScopeClass];
    }
    return [[self.scopeClass alloc] initScopeWithParams:params];
}

@end
