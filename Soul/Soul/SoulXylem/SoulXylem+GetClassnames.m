//
//  SoulXylem+GetClassnames.m
//  Soul
//
//  Created by 徐腾浩 on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulXylem+GetClassnames.h"

@implementation SoulXylem (GetClassnames)

- (Class)getDefaultScopeClass {
    NSString *soulname = NSStringFromClass(self.class);
    soulname = [soulname stringByReplacingOccurrencesOfString:@"Xylem" withString:@""];
    soulname = [soulname stringByAppendingString:@"Scope"];
    Class classname = NSClassFromString(soulname);
    return classname;
}
@end
