    //
    //  SoulXylem.h
    //  EvolutionForSoul
    //
    //  Created by 徐腾浩 on 4/1/16.
    //  Copyright © 2016 daopr. All rights reserved.
    //

#import "SoulScope.h"

@class RACSignal;


@interface SoulXylem : UIViewController
@property (nonatomic, assign) Class scopeClass;

@property (nonatomic, assign) BOOL *isSoul;

@property (nonatomic, strong) UIView *background;

@property (nonatomic, strong) SoulScope *scope;

@property (nonatomic, strong) SoulBlock soulWillAppear;
@property (nonatomic, strong) SoulBlock soulWillDisappear;
@property (nonatomic, strong) SoulBlock soulDidLoad;

@property (nonatomic, strong) SoulBlock soulMemoryWarning;
@property (nonatomic, strong) SoulBlock soulDealloc;

@property (nonatomic, strong) RACSignal *soul;

- (void)initSoul;
- (void)soulInit;
- (id)initWithRouterParams:(NSDictionary *)params;



@end
