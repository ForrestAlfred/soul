//
//  SoulTableView.h
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoulViewProtocol.h"
#import "SoulViewDelegate.h"

@interface SoulTableView : UITableView
<SoulViewProtocol>
@property (nonatomic, unsafe_unretained) id<SoulViewProtocol> soulViewDelegate;
@property (nonatomic, strong) SoulViewDelegate *soulViewDelegateImplement;
@end
