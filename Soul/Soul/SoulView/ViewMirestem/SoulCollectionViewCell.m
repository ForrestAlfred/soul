//
//  SoulCollectionViewCell.m
//  Soul
//
//  Created by DaoPr on 4/7/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulCollectionViewCell.h"
#import "SoulMaskImageView.h"
#import "Masonry.h"

@implementation SoulCollectionViewCell

- (void)initSoul {
	
}

- (void)soulInit {
	
}

- (void)soulDidInit {
	
}

    // convenience
-(void)setSoulMaskView:(UIView *)maskView {
    [self addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

-(void)setSoulMaskImage:(UIImage *)maskImage {
    SoulMaskImageView *maskView = [[SoulMaskImageView alloc]initWithImage:maskImage];
    [self setSoulMaskView:maskView];
}

-(void)deleteSoulMaskView {
    for (UIView *view in self.subviews) {
        if ([view.class isSubclassOfClass:[SoulMaskImageView class]]) {
            [view removeFromSuperview];
        }
    }
}
@end
