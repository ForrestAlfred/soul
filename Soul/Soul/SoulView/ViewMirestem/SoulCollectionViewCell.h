//
//  SoulCollectionViewCell.h
//  Soul
//
//  Created by DaoPr on 4/7/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoulBlocks.h"

@interface SoulCollectionViewCell : UICollectionViewCell
@property (nonatomic,   weak) UIView      *background;
@property (nonatomic, strong) NSDictionary *params;

@property (nonatomic, strong) NSString *tagString;
@property (nonatomic, assign) BOOL ifEverLoaded;

@property (nonatomic, strong) SoulBlock soulWillAppear;
@property (nonatomic, strong) SoulBlock soulWillDisappear;
@property (nonatomic, strong) SoulBlock soulDidLoad;

@property (nonatomic, strong) SoulBlock soulMemoryWarning;

- (void)initSoul;
- (void)soulInit;
- (void)soulDidInit;

    // convenience
-(void)setSoulMaskView:(UIView *)maskView;
-(void)setSoulMaskImage:(UIImage *)maskImage;
-(void)deleteSoulMaskView;
// delegate tbd
@end
