//
//  SoulCollectionView.m
//  PureCollectionView
//
//  Created by DaoPr on 4/7/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulCollectionView.h"

@implementation SoulCollectionView

- (id<SoulViewProtocol>)soulViewDelegate {
    if(!_soulViewDelegate) {
        _soulViewDelegate = self.soulViewDelegateImplement;
        self.soulViewDelegateImplement.consignor = self;
    }
    return _soulViewDelegate;
}

- (SoulViewDelegate *)soulViewDelegateImplement {
    if(_soulViewDelegateImplement == nil) {
        _soulViewDelegateImplement = [[SoulViewDelegate alloc] init];
    }
    return _soulViewDelegateImplement;
}

- (id)forwardingTargetForSelector:(SEL)aSelector {
    return self.soulViewDelegate;
}

//- (instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout {
//    self = [super initWithFrame:frame collectionViewLayout:layout];
//    [self initSoul];
//    [self soulInit];
//    return self;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)initSoul {
	
}

- (void)soulInit {
	
}

- (void)soulDidInit {
	
}

@end
