//
//  SoulTableView.m
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import "SoulTableView.h"
#import <objc/runtime.h>
@implementation SoulTableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
//
//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        [self initSoul];
//        [self soulInit];
//    }
//    return self;
//}

- (id<SoulViewProtocol>)soulViewDelegate {
    if(!_soulViewDelegate) {
        _soulViewDelegate = self.soulViewDelegateImplement;
        self.soulViewDelegateImplement.consignor = self;
    }
    return _soulViewDelegate;
}

- (SoulViewDelegate *)soulViewDelegateImplement {
    if(_soulViewDelegateImplement == nil) {
        _soulViewDelegateImplement = [[SoulViewDelegate alloc] init];
    }
    return _soulViewDelegateImplement;
}

- (id)forwardingTargetForSelector:(SEL)aSelector {
    return self.soulViewDelegate;
}

@end
