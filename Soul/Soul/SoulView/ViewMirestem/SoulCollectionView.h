    //
    //  SoulCollectionView.h
    //  PureCollectionView
    //
    //  Created by DaoPr on 4/7/16.
    //  Copyright © 2016 daopr. All rights reserved.
    //
#import <UIKit/UIKit.h>
#import "SoulBlocks.h"
#import "SoulViewProtocol.h"
#import "SoulViewDelegate.h"
#import "SoulView.h"

@interface SoulCollectionView :UICollectionView
<SoulViewProtocol>
@property (nonatomic, unsafe_unretained) id<SoulViewProtocol> soulViewDelegate;
@property (nonatomic, strong) SoulViewDelegate *soulViewDelegateImplement;

@property (nonatomic, strong) NSDictionary *params;
@property (nonatomic,   weak) SoulView      *background;
@property (nonatomic, strong) NSDictionary<NSString *,UIView *> *sharedViewsDictionary;

@property (nonatomic, assign) NSString *tagString;
@property (nonatomic, assign) BOOL ifEverLoaded;

@property (nonatomic, strong) UICollectionViewLayout *layout;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) SoulBlock soulWillAppear;
@property (nonatomic, strong) SoulBlock soulWillDisappear;
@property (nonatomic, strong) SoulBlock soulDidLoad;

@property (nonatomic, strong) SoulBlock soulMemoryWarning;

- (void)initSoul;
- (void)soulInit;
- (void)soulDidInit;
@end
