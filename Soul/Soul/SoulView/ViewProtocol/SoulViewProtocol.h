//
//  SoulViewProtocol.h
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoulBlocks.h"

@protocol SoulViewProtocol
@required
@property (nonatomic, strong) NSDictionary *params;

@property (nonatomic,   strong) UIView      *background;
@property (nonatomic,   strong) UIView      *contentView;

@property (nonatomic, strong) NSDictionary<NSString *,UIView *> *sharedViewsDictionary;

    //cell
@property (nonatomic, assign) NSString *tagString;
@property (nonatomic, assign) BOOL ifEverLoaded;

@property (nonatomic, strong) SoulBlock soulWillAppear;
@property (nonatomic, strong) SoulBlock soulWillDisappear;
@property (nonatomic, strong) SoulBlock soulDidLoad;

@property (nonatomic, strong) SoulBlock soulMemoryWarning;

- (void)initSoul;
- (void)soulInit;
- (void)soulDidInit;

    // convenience
-(void)setSoulMaskView:(UIView *)maskView;
-(void)setSoulMaskImage:(UIImage *)maskImage;
-(void)deleteSoulMaskView;

@optional
@property CFTimeInterval beginTime;
@end
