//
//  SoulViewDelegate.m
//  Hime
//
//  Created by DaoPr on 4/17/16.
//  Copyright © 2016 Leedss. All rights reserved
//
#import <UIKit/UIKit.h>
#import "SoulViewDelegate.h"
#import "SoulView.h"
#import "Masonry.h"
#import "SoulMaskView.h"
#import "SoulMaskImageView.h"
#import "LxDBAnything.h"

@implementation SoulViewDelegate
@synthesize background = _background;

- (void)initSoul {
    
}

- (void)soulInit {
    
}

- (void)soulDidInit {
    
}


    // convenience
-(void)setSoulMaskView:(UIView *)maskView {
    [self.consignor addSubview:maskView];
    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.consignor);
    }];
}

-(void)setSoulMaskImage:(UIImage *)maskImage {
    SoulMaskImageView *maskView = [[SoulMaskImageView alloc]initWithImage:maskImage];
    [self.consignor performSelector:@selector(setSoulMaskView:) withObject:maskView];
}

-(void)deleteSoulMaskView {
    for (UIView *view in self.subviews) {
        if ([view.class isSubclassOfClass:[SoulMaskImageView class]]) {
            [view removeFromSuperview];
        }
    }
}



//- (void)setBackground:(UIView *)background{
//    _background = background;
//}
//- (UIView *)background {
//    return _background;
//}

@end
