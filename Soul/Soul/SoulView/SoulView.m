    //
    //  SoulView.m
    //  Hime
    //
    //  Created by DaoPr on 4/6/16.
    //  Copyright © 2016 Leedss. All rights reserved.
    //
#import <objc/runtime.h>
#import "SoulView.h"

//static char kBackgroundObjectKey;
//static char kSoulWillAppearObjectKey;
//static char kSoulWillDisappearObjectKey;
//static char kSoulDidLoadObjectKey;

@implementation SoulView
//@dynamic background;
//@dynamic soulWillAppear;
//@dynamic soulWillDisappear;
//@dynamic soulDidLoad;
//
//- (void)setBackground:(id)object {
//    objc_getAssociatedObject(self, &kBackgroundObjectKey);
//    objc_setAssociatedObject(self, @selector(background), object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}
//
//- (id)background {
//    objc_getAssociatedObject(self, &kBackgroundObjectKey);
//    return objc_getAssociatedObject(self, @selector(background));
//}
//
//- (void)setSoulWillAppear:(SoulBlock)object {
//    objc_getAssociatedObject(self, &kSoulWillAppearObjectKey);
//    objc_setAssociatedObject(self, @selector(soulWillAppear), object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    
//}
//- (SoulBlock)soulWillAppear {
//    objc_getAssociatedObject(self, &kSoulWillAppearObjectKey);
//    return objc_getAssociatedObject(self, @selector(soulWillAppear));
//}
//
//- (void)setSoulWillDisappear:(SoulBlock)object {
//    objc_getAssociatedObject(self, &kSoulWillAppearObjectKey);
//    objc_setAssociatedObject(self, @selector(soulWillDisappear), object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    
//}
//- (SoulBlock)soulWillDisappear {
//    objc_getAssociatedObject(self, &kSoulWillDisappearObjectKey);
//    return objc_getAssociatedObject(self, @selector(soulWillDisappear));
//}
//
//- (void)setSoulDidLoad:(SoulBlock)object {
//    objc_getAssociatedObject(self, &kSoulWillAppearObjectKey);
//    objc_setAssociatedObject(self, @selector(soulDidLoad), object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    
//}
//- (SoulBlock)soulDidLoad {
//    objc_getAssociatedObject(self, &kSoulDidLoadObjectKey);
//    return objc_getAssociatedObject(self, @selector(soulDidLoad));
//}
//
//- (void)initSoul {
//    [self.soulViewDelegate initSoul];
//}
//
//- (void)soulInit {
//    
//}
//
//- (void)soulDidInit {
//    
//}

//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        [self initSoul];
//        [self soulInit];
//    }
//    return self;
//}
    // convenience
//-(void)setSoulMaskView:(UIView *)maskView {
//    [self addSubview:maskView];
//    [maskView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self);
//    }];
//}
//
//-(void)setSoulMaskImage:(UIImage *)maskImage {
//    SoulMaskImageView *maskView = [[SoulMaskImageView alloc]initWithImage:maskImage];
//    [self setSoulMaskView:maskView];
//}
//
//-(void)deleteSoulMaskView {
//    for (UIView *view in self.subviews) {
//        if ([view.class isSubclassOfClass:[SoulMaskImageView class]]) {
//            [view removeFromSuperview];
//        }
//    }
//}

//- (id)copyWithZone:(NSZone *)zone {
//    return [super copy];
//}

- (id<SoulViewProtocol>)soulViewDelegate {
    if(!_soulViewDelegate) {
        _soulViewDelegate = self.soulViewDelegateImplement;
        self.soulViewDelegateImplement.consignor = self;
    }
    return _soulViewDelegate;
}

- (SoulViewDelegate *)soulViewDelegateImplement {
    if(_soulViewDelegateImplement == nil) {
        _soulViewDelegateImplement = [[SoulViewDelegate alloc] init];
    }
    return _soulViewDelegateImplement;
}

- (id)forwardingTargetForSelector:(SEL)aSelector {
    return self.soulViewDelegate;
}
//
//+ (BOOL)resolveInstanceMethod:(SEL)sel {
//    
//    NSString *selectorString = NSStringFromSelector(sel);
//    SEL selector = NSSelectorFromString(selectorString);
//
//    if (!class_respondsToSelector(self.class, selector)) {
//        
//    }
//  
//    return [super resolveInstanceMethod:sel];
//    
//}
//
//void functionForMethod1(id self, SEL _cmd) {
//    
//    NSLog(@"%@, %p", self, _cmd);

//}

@end
