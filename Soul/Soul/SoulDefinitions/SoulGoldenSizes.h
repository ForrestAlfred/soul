//
//  Soulstatic const double GoldenenSizes.h
//  Hime
//
//  Created by DaoPr on 4/21/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#ifndef Soulstatic const double GoldenenSizes_h
#define Soulstatic const double GoldenenSizes_h
static const double Golden1 = 1;
static const double Golden2 = 2;
static const double Golden3 = 3;
static const double Golden5 = 5;
static const double Golden8 = 8;

static const double Golden13 = 13;
static const double Golden21 = 21;
static const double Golden34 = 34;
static const double Golden55 = 55;
static const double Golden89 = 89;

static const double Golden144 = 144;
static const double Golden233 = 233;
static const double Golden377 = 377;
static const double Golden610 = 610;
static const double Golden987 = 987;

static const double Golden1597 = 1597;
static const double Golden2584 = 2584;
static const double Golden4181 = 4181;
static const double Golden6765 = 6765;

static const double Golden10946 = 10946;
static const double Golden17711 = 17711;
static const double Golden28657 = 28657;
static const double Golden46368 = 46368;
static const double Golden75025 = 75025;

// Time
// 1K Micro Second
static const double GoldenD = 0.1;
static const double GoldenH = 0.01;
static const double GoldenK = 0.001;
static const double Golden10K = 0.0001;

static const double GoldenMillisecondD = 0.1;
static const double GoldenMillisecondH = 0.01;
static const double GoldenMillisecondK = 0.001;
static const double GoldenMillisecond10K = GoldenMillisecondK * 0.1;
static const double GoldenMillisecond100K = GoldenMillisecond10K * 0.1;
static const double GoldenMillisecond1000K = GoldenMillisecond100K * 0.1;
static const double GoldenMillisecond10000K = GoldenMillisecond1000K * 0.1;

//static const double GoldenMillisecondFor1000K = 0.000001;
// 1ms ~ 10ms
static const double GUM1ms = Golden1 * GoldenMillisecondK;
static const double GUM2ms = Golden2 * GoldenMillisecondK;
static const double GUM3ms = Golden3 * GoldenMillisecondK;
static const double GUM5ms = Golden5 * GoldenMillisecondK;
static const double GUM8ms = Golden8 * GoldenMillisecondK;
static const double GUM1msR3 = Golden13 * GoldenMillisecond10K;
static const double GUM2msR1 = Golden21 * GoldenMillisecond10K;
static const double GUM3msR4 = Golden34 * GoldenMillisecond10K;
static const double GUM5msR5 = Golden55 * GoldenMillisecond10K;
static const double GUM8msR9 = Golden89 * GoldenMillisecond10K;
static const double GUM1msR44 = Golden144 * GoldenMillisecond100K;
static const double GUM2msR33 = Golden233 * GoldenMillisecond100K;;
static const double GUM3msR77 = Golden377 * GoldenMillisecond100K;
static const double GUM6msR10 = Golden610 * GoldenMillisecond100K;
static const double GUM9msR87 = Golden987 * GoldenMillisecond100K;
static const double GUM1msR597 = Golden1597 * GoldenMillisecond1000K;
static const double GUM2msR584 = Golden2584 * GoldenMillisecond1000K;
static const double GUM4msR181 = Golden4181 * GoldenMillisecond1000K;
static const double GUM6msR765 = Golden6765 * GoldenMillisecond1000K;
static const double GUM1msR0946 = Golden10946 * GoldenMillisecond10000K;
static const double GUM1msR7711 = Golden17711 * GoldenMillisecond10000K;
static const double GUM2msR8657 = Golden28657 * GoldenMillisecond10000K;
static const double GUM4msR6368 = Golden46368 * GoldenMillisecond10000K;
static const double GUM7msR5025 = Golden75025 * GoldenMillisecond10000K;
// 10ms ~ 100ms
static const double GDM10ms = Golden1 * GoldenMillisecondH;
static const double GDM20ms = Golden5 * GoldenMillisecondH;
static const double GDM30ms = Golden3 * GoldenMillisecondH;
static const double GDM50ms = Golden5 * GoldenMillisecondH;
static const double GDM80ms = Golden8 * GoldenMillisecondH;
static const double GDM13ms = Golden13 * GoldenMillisecondK;
static const double GDM21ms = Golden21 * GoldenMillisecondK;
static const double GDM34ms = Golden34 * GoldenMillisecondK;
static const double GDM55ms = Golden55 * GoldenMillisecondK;
static const double GDM89ms = Golden89 * GoldenMillisecondK;
static const double GDM14msR4 = Golden144 * GoldenMillisecond10K;
static const double GDM23msR3 = Golden233 * GoldenMillisecond10K;
static const double GDM37msR7 = Golden377 * GoldenMillisecond10K;
static const double GDM61ms = Golden610 * GoldenMillisecond10K;
static const double GDM98msR7 = Golden987 * GoldenMillisecond10K;
static const double GDM15msR97 = Golden1597 * GoldenMillisecond100K;
static const double GDM25msR84 = Golden2584 * GoldenMillisecond100K;
static const double GDM41msR81 = Golden4181 * GoldenMillisecond100K;
static const double GDM67msR65 = Golden6765 * GoldenMillisecond100K;
static const double GDM10msR946 = Golden10946 * GoldenMillisecond1000K;
static const double GDM17msR711 = Golden17711 * GoldenMillisecond1000K;
static const double GDM28msR657 = Golden28657 * GoldenMillisecond1000K;
static const double GDM46msR368 = Golden46368 * GoldenMillisecond1000K;
static const double GDM75msR025 = Golden75025 * GoldenMillisecond1000K;
    // 100ms ~ 1000ms
static const double GHM100ms = Golden1 * GoldenMillisecondD;
static const double GHM200ms = Golden2 * GoldenMillisecondD;
static const double GHM300ms = Golden3 * GoldenMillisecondD;
static const double GHM500ms = Golden5 * GoldenMillisecondD;
static const double GHM800ms = Golden8 * GoldenMillisecondD;
static const double GHM130ms = Golden13 * GoldenMillisecondH;
static const double GHM210ms = Golden21 * GoldenMillisecondH;
static const double GHM340ms = Golden34 * GoldenMillisecondH;
static const double GHM550ms = Golden55 * GoldenMillisecondH;
static const double GHM890ms = Golden89 * GoldenMillisecondH;
static const double GHM144ms = Golden144 * GoldenMillisecondK;
static const double GHM233ms = Golden233 * GoldenMillisecondK;
static const double GHM377ms = Golden377 * GoldenMillisecondK;
static const double GHM610ms = Golden610 * GoldenMillisecondK;
static const double GHM987ms = Golden987 * GoldenMillisecondK;
static const double GHM159msR7 = Golden1597 * GoldenMillisecond10K;
static const double GHM258msR4 = Golden2584 * GoldenMillisecond10K;
static const double GHM418msR1 = Golden4181 * GoldenMillisecond10K;
static const double GHM676msR5 = Golden6765 * GoldenMillisecond10K;
static const double GHM109msR46 = Golden10946 * GoldenMillisecond100K;
static const double GHM177msR11 = Golden17711 * GoldenMillisecond100K;
static const double GHM286msR57 = Golden28657 * GoldenMillisecond100K;
static const double GHM463msR68 = Golden46368 * GoldenMillisecond100K;
static const double GHM750msR25 = Golden75025 * GoldenMillisecond100K;
// 1s ~ 10s
static const double GUS1s = Golden1 ;
static const double GUS2s = Golden2 ;
static const double GUS3s = Golden3 ;
static const double GUS5s = Golden5 ;
static const double GUS8s = Golden8 ;
static const double GUS1sR3 = Golden13 * GoldenD;
static const double GUS2sR1 = Golden21 * GoldenD;
static const double GUS3sR4 = Golden34 * GoldenD;
static const double GUS5sR5 = Golden55 * GoldenD;
static const double GUS8sR9 = Golden89 * GoldenD;
static const double GUS1sR44 = Golden144 * GoldenH;
static const double GUS2sR33 = Golden233 * GoldenH;;
static const double GUS3sR77 = Golden377 * GoldenH;
static const double GUS6sR10 = Golden610 * GoldenH;
static const double GUS9sR87 = Golden987 * GoldenH;
static const double GUS1sR597 = Golden1597 * GoldenK;
static const double GUS2sR584 = Golden2584 * GoldenK;
static const double GUS4sR181 = Golden4181 * GoldenK;
static const double GUS6sR765 = Golden6765 * GoldenK;
static const double GUS1sR0946 = Golden10946 * Golden10K;
static const double GUS1sR7711 = Golden17711 * Golden10K;
static const double GUS2sR8657 = Golden28657 * Golden10K;
static const double GUS4sR6368 = Golden46368 * Golden10K;
static const double GUS7sR5025 = Golden75025 * Golden10K;

#endif /* Soulstatic const double GoldenenSizes_h */
