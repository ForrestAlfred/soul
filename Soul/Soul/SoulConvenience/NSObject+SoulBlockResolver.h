//
//  NSObject+SoulBlockResolver.h
//  Hime
//
//  Created by DaoPr on 4/16/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoulBlocks.h"

@interface NSObject (SoulBlockResolver)
- (void)executeSoulBlock:(SoulBlock)block;
- (void)executeSoulBlock:(SoulParamsBlock)block params:(NSDictionary *)params;
@end
