//
//  NSObject+SoulBlockResolver.m
//  Hime
//
//  Created by DaoPr on 4/16/16.
//  Copyright © 2016 Leedss. All rights reserved.
//

#import "NSObject+SoulBlockResolver.h"

@implementation NSObject (SoulBlockResolver)

- (void)executeSoulBlock:(SoulBlock)block {
    block?block():nil;
}

- (void)executeSoulBlock:(SoulParamsBlock)block params:(NSDictionary *)params {
    block?block(params):nil;
}


@end
