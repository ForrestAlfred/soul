//
//  SoulScope+GetClassnames.m
//  Soul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope+GetClassnames.h"

@implementation SoulScope (GetClassnames)

- (Class)getDefaultViewClass {
    NSString *viewname = NSStringFromClass(self.class);
    viewname = [viewname stringByReplacingOccurrencesOfString:@"Scope" withString:@""];
    viewname = [viewname stringByAppendingString:@"View"];
    Class viewClass = NSClassFromString(viewname);
    
    return viewClass;
}

- (Class)getDefaultMissueClass {
    NSString *viewname = NSStringFromClass(self.class);
    viewname = [viewname stringByReplacingOccurrencesOfString:@"Scope" withString:@""];
    viewname = [viewname stringByAppendingString:@"Missue"];
    Class viewClass = NSClassFromString(viewname);
    
    return viewClass;
}

- (Class)getDefaultListScopeClass {
    NSString *viewname = NSStringFromClass(self.class);
    viewname = [viewname stringByReplacingOccurrencesOfString:@"Scope" withString:@""];
    viewname = [viewname stringByAppendingString:@"ListScope"];
    Class viewClass = NSClassFromString(viewname);
    
    return viewClass;
}

- (Class)getDefaultCollectionViewLayoutClass {
    NSString *viewname = NSStringFromClass(self.class);
    viewname = [viewname stringByReplacingOccurrencesOfString:@"Scope" withString:@""];
    viewname = [viewname stringByAppendingString:@"Layout"];
    Class viewClass = NSClassFromString(viewname);
    
    return viewClass;
}
@end
