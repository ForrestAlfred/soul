//
//  SoulScope+SoulSubmodulesResolver.m
//  Soul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope+SoulSubmodulesResolver.h"

#import "SoulScope+GetClassnames.h"
#import "SoulScope+SoulScopeResolver.h"
#import "SoulCollectionView.h"
#import "SoulCollectionViewCell.h"

#import <ReactiveCocoa.h>
@implementation SoulScope (SoulSubmodulesResolver)
- (id)resolveView {
    SoulView* soulView;
    if (!self.viewClass) {
        self.viewClass = [self getDefaultViewClass];
    }
    
    if ([self.viewClass isSubclassOfClass:[UICollectionView class]]) {
        SoulCollectionView* view = [self resolveCollectionView];
        view.layout = self.collectionViewlayout;
        view.flowLayout = view.layout;
        self.collectionView = view;

        soulView = view;
    }
    else
    if ([self.viewClass isSubclassOfClass:[UICollectionViewCell class]]) {
        SoulCollectionView* cellView = [self resolveCollectionCellView];
       soulView = cellView;
    }
    else {
        soulView = [[self.viewClass alloc] init];
    }
    
    [self resolveSoulViewInitMetod:soulView];
    
    return soulView;
}
- (id)resolveCollectionView {
    if (!self.collectionViewlayout) {
        if ([self getDefaultCollectionViewLayoutClass]) {
            Class collectionViewlayoutClass = [self getDefaultCollectionViewLayoutClass];
            self.collectionViewlayout = [collectionViewlayoutClass new];
        } else {
            UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
            [flowLayout setScrollDirection:self.scrolldirection];
            self.collectionViewlayout = flowLayout;
        }
    }
    return [[self.viewClass alloc] initWithFrame:self.view.bounds collectionViewLayout:self.collectionViewlayout];
}

- (id)resolveCollectionCellView {
    UICollectionView *collectionView = self.background;
    
    SoulCollectionViewCell* cell =  [collectionView   dequeueReusableCellWithReuseIdentifier:NSStringFromClass([self getDefaultViewClass]) forIndexPath:self.indexPath];
 
    return cell;
}

- (void)resolveSoulViewInitMetod:(SoulView *)soulView {
   soulView.params = self.params;
    soulView.background = self.background;
    [self.background addSubview:soulView];
    [soulView initSoul];
    [soulView soulInit];
}

- (id)resolveMissueWithParams:(NSDictionary *)params {
    if (!self.missueClass) {
        self.missueClass = [self getDefaultMissueClass];
    }
    return  [[self.missueClass alloc] initMissueWithParams:params];
}

- (id)resolveListScope {
    if (!self.listScope) {
        self.listScopeClass = [self getDefaultListScopeClass];
    }
    return [[self.listScopeClass alloc] initNoneSettingScopeWithParams:self.params baseScope:self];
}

- (instancetype)initNoneSettingScopeWithParams:(NSDictionary *)params baseScope:(SoulScope *)baseScope
{
    return [self initListScopeWithParams:params baseScope:baseScope];
}
-(instancetype)initListScopeWithParams:(NSDictionary *)params baseScope:(SoulScope *)baseScope {
    if (!(self = [super init]))   {return nil;}
    
    @weakify(params);
    self.soul = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(params);
        [subscriber sendNext:params];
        [subscriber sendCompleted];
        return nil;
    }];
    
    self.baseScope = baseScope;
    self.params = params;
    
    [self initSoul];
    [self soulInit];
    [self soulDidInit];
    return self;
}


@end
