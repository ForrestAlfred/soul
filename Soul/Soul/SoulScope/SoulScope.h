    //
    //  SoulScope.h
    //  EvolutionForSoul
    //
    //  Created by 徐腾浩 on 4/1/16.
    //  Copyright © 2016 daopr. All rights reserved.
    //

#import "SoulMissue.h"
#import "SoulView.h"
#import "SoulCollectionView.h"

@class RACSignal;

@interface SoulScope : NSObject
<
UITableViewDelegate,
UITableViewDataSource,
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,
UIScrollViewDelegate,
UIScrollViewAccessibilityDelegate
>

@property (nonatomic, assign) Class missueClass;
@property (nonatomic, assign) Class viewClass;
@property (nonatomic, strong) Class listScopeClass;

@property (nonatomic, strong) NSDictionary *params;

@property (nonatomic, strong) UIView      *background;
@property (nonatomic, strong) SoulView    *view;
@property (nonatomic, strong) SoulCollectionView    *collectionView;

@property (nonatomic, strong) NSArray<SoulScope *> *subScopes;
@property (nonatomic, strong) NSDictionary<NSString *,UIView *> *sharedViewsDictionary;

@property (nonatomic, strong) NSArray<Class> *subScopeClasses;
@property (nonatomic, strong) NSMutableDictionary<NSString *,SoulScope *> *sharedScopesDictionary;
@property (nonatomic, strong) NSMutableArray<SoulScope *> *sharedScopesMArray;


@property (nonatomic, strong) SoulMissue  *missue;

@property (nonatomic, strong) SoulScope   *baseScope;
@property (nonatomic, strong) SoulScope   *listScope;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@property (nonatomic, strong) RACSignal *cell;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, assign) NSString *tagString;
@property (nonatomic, strong) NSMutableDictionary<NSString *,SoulScope *> *cellScopesDictionary;
@property (nonatomic, assign) BOOL ifEverLoaded;

@property (nonatomic, strong) UICollectionViewLayout *collectionViewlayout;
@property (nonatomic, assign) UICollectionViewScrollDirection scrolldirection;

@property (nonatomic, strong) SoulBlock soulWillAppear;
@property (nonatomic, strong) SoulBlock soulWillDisappear;
@property (nonatomic, strong) SoulBlock soulDidLoad;

@property (nonatomic, strong) SoulBlock soulMemoryWarning;

@property (nonatomic, strong) RACSignal *soul;

- (void)soulWillAppearMethod;
- (void)soulWillDisappearMethod;
- (void)soulDidLoadMethod;
- (void)soulMemoryWarningMethod;

- (void)initSoul;
- (void)soulInit;
- (void)soulDidInit;

-(instancetype)initScopeWithParams:(NSDictionary *)params;
-(instancetype)initScopeWithParams:(NSDictionary *)params background:(SoulView *)background;
-(instancetype)initScopeWithParams:(NSDictionary *)params collectionView:(UICollectionView*)collectionView indexPath:(NSIndexPath *)indexPath;

@end
