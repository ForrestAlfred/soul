//
//  SoulScope+SoulViewResolver.m
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope+SoulViewResolver.h"
#import "SoulBlocks.h"
#import "SoulScope+GetClassnames.h"
#import "SoulScope+SoulScopeResolver.h"

@implementation SoulScope (SoulViewResolver)
- (void)soulWillAppearSubScopeBlockResolve {
    if (self.subScopes) {
        for (SoulScope *scope in self.subScopes) {
            [scope soulWillAppearMethod];
        }
    }
}
- (void)soulWillDisappearSubScopeBlockResolve {
    if (self.subScopes) {
        for (SoulScope *scope in self.subScopes) {
            [scope soulWillDisappearMethod];
        }
    }
}
- (void)soulDidLoadSubScopeBlockResolve {
    if (self.subScopes) {
        for (SoulScope *scope in self.subScopes) {
            [scope soulDidLoadMethod];
        }
    }
}

- (void)soulWillAppearBlockResolve:(SoulScope *)target {
    [self soulBlockResolve:target.soulWillAppear];
    
    [self soulBlockResolve:target.listScope.soulWillAppear];
    
    [self soulBlockResolve:target.view.soulWillAppear];
    
    [self soulBlockResolve:target.missue.soulWillAppear];
    
    [self.background addSubview:self.view];
}
- (void)soulWillDisappearBlockResolve:(SoulScope *)target {
    [self soulBlockResolve:target.soulWillDisappear];
    
    [self soulBlockResolve:target.listScope.soulWillDisappear];
    
    [self soulBlockResolve:target.view.soulWillDisappear];
    
    [self soulBlockResolve:target.missue.soulWillDisappear];
}
- (void)soulDidLoadBlockResolve:(SoulScope *)target {
    [self soulBlockResolve:target.soulDidLoad];
    
    [self soulBlockResolve:target.listScope.soulDidLoad];
    
    [self soulBlockResolve:target.view.soulDidLoad];
    
    [self soulBlockResolve:target.missue.soulDidLoad];
}



//- (void)soulBackgroundResolve {
//    if ([self.viewClass isSubclassOfClass:[UICollectionViewCell class]]) {
//        self.background = self.
//    }
//}

- (void)soulSubscopesBackgroundResolve {
    if (self.subScopes) {
        for (SoulScope *scope in self.subScopes) {
            [self soulSubscopesBackgroundResolve:scope];
        }
    }
}

- (void)soulSubscopesBackgroundResolve:(SoulScope *)target {

//    if ([[self getDefaultViewClass] isSubclassOfClass:[UICollectionViewCell class]]) {
//        UICollectionViewCell *cell = self.view;
//        
//        target.background = cell.contentView;
//        target.view.background = cell.contentView;
//    } else {
        target.background = self.view;
        target.view.background = self.view;
//    }
    [target.background addSubview:target.view];
}

@end
