//
//  SoulScope+Soul.m
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/2/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope+Soul.h"
#import <ReactiveCocoa.h>

@implementation SoulScope (Soul)

- (void)view:(Class)viewClass
      missue:(Class)missueClass
   listScope:(Class)listScope {
   
    self.missueClass    = [missueClass class];
    self.viewClass      = [viewClass class];
    self.listScopeClass = [listScope class];
    
   }

- (void)init:(SoulInitBlock)soulInitBlock {
    [self.soul subscribeNext:soulInitBlock];
}

- (void)cellForRow:(SoulCellBlock)soulCellBlock {
    [RACObserve(self, cell) subscribeNext:^(id x) {
        [self.cell subscribeNext:soulCellBlock];
    }];
}

@end
