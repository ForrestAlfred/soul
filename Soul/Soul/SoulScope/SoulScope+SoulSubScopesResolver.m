//
//  SoulScope+SoulSubScopesResolver.m
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope+SoulSubScopesResolver.h"


@implementation SoulScope (SoulSubScopesResolver)
- (void)subScopesResolverWithParams:(NSDictionary *)params {
    NSMutableArray* scopeMArray = [NSMutableArray new];
    NSMutableDictionary* scopeMdictionary = [NSMutableDictionary new];
    
    NSMutableDictionary* scopeViewMdictionary = [NSMutableDictionary new];
    
    self.subScopes = scopeMArray;
    
    self.sharedScopesDictionary = scopeMdictionary;
    
    self.sharedViewsDictionary = scopeViewMdictionary;
    
    for (int i = 0; i< self.subScopeClasses.count; i++) {

        Class scopeClass = self.subScopeClasses[i];
        SoulScope* scope = [[scopeClass alloc]initScopeWithParams:params background:self.view];
        
       [scopeMArray addObject:scope];
        if (scope.view) {
            [scopeViewMdictionary setObject:scope.view forKey:NSStringFromClass([scope.view class])];
        }
      
        if (scopeClass) {
            [scopeMdictionary setObject:scope forKey:NSStringFromClass(scopeClass)];
        }
    }

    for ( SoulScope* scope in self.subScopes) {
        scope.sharedScopesMArray = scopeMArray;
        scope.sharedScopesDictionary = self.sharedScopesDictionary;
        scope.view.sharedViewsDictionary = self.sharedViewsDictionary;
    }
    
}
//- (void)setupSubScopeBackgrounds {
//    for (int i = 0; i< self.subScopes.count; i++) {
//        SoulScope* scope = self.subScopes[i];
//        scope.background = self.view;
//    }
//}
@end
