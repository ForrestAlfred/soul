//
//  SoulScope+TableScope.m
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope+TableScope.h"
#import <ReactiveCocoa.h>

@implementation SoulScope (TableScope)
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __block UITableViewCell *cell = [UITableViewCell new];
    cell.accessoryType = UITableViewCellStyleValue1;
    cell.backgroundColor = [UIColor yellowColor];

    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"S";
            break;
        case 1:
            cell.textLabel.text = @"O";
            break;
        case 2:
            cell.textLabel.text = @"U";
            break;
        case 3:
            cell.textLabel.text = @"L";
            break;
        default:
            break;
    }
    
    self.indexPath = indexPath;
    
    self.cell = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [subscriber sendNext:cell];
        [subscriber sendCompleted];
        return nil;
    }];

 
    return cell;
}
@end
