//
//  SoulScope+CollectionScope.m
//  Soul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope+CollectionScope.h"
#import "SoulCollectionViewCell.h"

@implementation SoulScope (CollectionScope)
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    SoulCollectionViewCell* soulCell = cell;
    [soulCell initSoul];
    [soulCell soulInit];
    [soulCell soulDidInit];
}
@end
