//
//  SoulScope+GetClassnames.h
//  Soul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope.h"

@interface SoulScope (GetClassnames)
- (Class)getDefaultViewClass;
- (Class)getDefaultMissueClass;
- (Class)getDefaultListScopeClass;

- (Class)getDefaultCollectionViewLayoutClass;
@end
