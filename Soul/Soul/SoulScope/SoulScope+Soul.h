//
//  SoulScope+Soul.h
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/2/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope.h"

@interface SoulScope (Soul)
- (void)view:(Class)viewClass
      missue:(Class)missueClass
     listScope:(Class)listScope;

- (void)init:(SoulInitBlock)soulInitBlock;

- (void)cellForRow:(SoulCellBlock)soulCellBlock;

@end
