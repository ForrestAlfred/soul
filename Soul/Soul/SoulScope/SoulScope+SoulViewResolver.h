//
//  SoulScope+SoulViewResolver.h
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope.h"

@interface SoulScope (SoulViewResolver)

- (void)soulWillAppearSubScopeBlockResolve;
- (void)soulWillDisappearSubScopeBlockResolve;
- (void)soulDidLoadSubScopeBlockResolve;


- (void)soulWillAppearBlockResolve:(SoulScope *)target;
- (void)soulWillDisappearBlockResolve:(SoulScope *)target;
- (void)soulDidLoadBlockResolve:(SoulScope *)target;

- (void)soulSubscopesBackgroundResolve;
@end
