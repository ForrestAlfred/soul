//
//  SoulScope+SoulScopeResolver.m
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope+SoulScopeResolver.h"

#import "SoulScope+SoulSubmodulesResolver.h"

#import "SoulScope+SoulViewResolver.h"
#import "SoulScope+SoulSubScopesResolver.h"

#import <ReactiveCocoa.h>

@implementation SoulScope (SoulScopeResolver)
-(instancetype)initScopeWithParams:(NSDictionary *)params collectionView:(UICollectionView*)collectionView indexPath:(NSIndexPath *)indexPath  {
    if (!(self = [super init]))   {return nil;}
    
    self.background = collectionView;
    self.indexPath = indexPath;
    
    [self initScopeCoreWithParams:params];
    return self;
}
-(instancetype)initScopeWithParams:(NSDictionary *)params {
    if (!(self = [super init]))   {return nil;}
    
    [self initScopeCoreWithParams:params];
    return self;
}
-(instancetype)initScopeWithParams:(NSDictionary *)params background:(SoulView *)background {
    if (!(self = [super init]))   {return nil;}
    self.background = background;
    [self initScopeCoreWithParams:params];
    return self;
}
- (void)initScopeCoreWithParams:(NSDictionary *)params {

    self.params = params;
 
    @weakify(params);
    self.soul = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(params);
        [subscriber sendNext:params];
        [subscriber sendCompleted];
        return nil;
    }];
    
    [self initSoul];
    [self soulInit];
    
        // for subscopes
    
    self.missue    = [self resolveMissueWithParams:params];
    
    self.view      = [self resolveView];
    
    [self subScopesResolverWithParams:params];
    
    [self soulSubscopesBackgroundResolve];
    
    self.listScope = [self resolveListScope];
    
//    if (self.params) {
//        self.listScope.params = self.params;
//    }
//    if (self.listScope) {
//        self.listScope.baseScope = self;
//    }
    
    [self submodulesTypeResolver];
    
    [self soulInit];
    [self soulDidInit];
}

- (void)submodulesTypeResolver {
    if (self.listScope) {
        

        self.listScope.view       = self.view;
        self.listScope.collectionView = self.view;
        
        self.listScope.background = self.background;
        
        self.listScope.missue     = self.missue;
        
        [self scopeDelegateResolver:self.listScope];
    } else {
        [self scopeDelegateResolver:self];
    }
}

- (void)scopeDelegateResolver:(SoulScope *)target {
    if ([self.view isKindOfClass:[UITableView class]]) {
        UITableView *soulView = self.view;
        soulView.delegate     = target;
        soulView.dataSource   = target;
    }
    if ([self.view isKindOfClass:[UICollectionView class]]) {
        UICollectionView *soulView    = self.view;
        soulView.delegate             = target;
        soulView.dataSource           = target;
    }
}

- (void)soulBlockResolve:(SoulBlock)soulBlock {
    soulBlock?soulBlock():nil;
}
@end
