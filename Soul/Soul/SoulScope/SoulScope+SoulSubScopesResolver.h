//
//  SoulScope+SoulSubScopesResolver.h
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope.h"

@interface SoulScope (SoulSubScopesResolver)

- (void)subScopesResolverWithParams:(NSDictionary *)params;
//- (void)setupSubScopeBackgrounds;
@end
