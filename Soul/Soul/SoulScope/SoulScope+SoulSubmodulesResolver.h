//
//  SoulScope+SoulSubmodulesResolver.h
//  Soul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope.h"

@interface SoulScope (SoulSubmodulesResolver)
- (id)resolveView;
- (id)resolveMissueWithParams:(NSDictionary *)params;
- (id)resolveListScope;



@end
