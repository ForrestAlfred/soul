    //
    //  SoulScope.m
    //  EvolutionForSoul
    //
    //  Created by 徐腾浩 on 4/1/16.
    //  Copyright © 2016 daopr. All rights reserved.
    //

#import "SoulScope.h"

#import "SoulScope+SoulScopeResolver.h"

#import "SoulScope+SoulViewResolver.h"

@implementation SoulScope

    // Example
-(void)initSoul {
//    [self init:^(NSDictionary *params) {
//            //        self.scopeClass = [xxxMissue class];
//    }];

        //    [self  view:[SoulView class]
        //         missue:[SoulATissue class]
        //      listScope:nil];
}

- (void)soulInit { }

- (void)soulDidInit { }

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Soul Warning"
                                   reason:@"SoulScope Init ItSelf"
                                 userInfo:nil];
    
}

- (void)soulWillAppearMethod {
    [self soulWillAppearBlockResolve:self];

    [self soulWillAppearSubScopeBlockResolve];
}

- (void)soulWillDisappearMethod {

    [self soulWillDisappearBlockResolve:self];
    
    [self soulWillDisappearSubScopeBlockResolve];
}

- (void)soulDidLoadMethod {
    [self soulDidLoadBlockResolve:self];
    
    [self soulDidLoadSubScopeBlockResolve];
}

- (void)soulMemoryWarningMethod {
//    [self soulBlockResolve:self.view.soulMemoryWarning];
//    
//    [self soulBlockResolve:self.missue.soulMemoryWarning];
}

- (void)setBackground:(UIView *)background {
    _background          = background;
    self.view.background = background;
    
    [self.background addSubview:self.view];

    [self soulSubscopesBackgroundResolve];
}

- (void)setParams:(NSDictionary *)params {
    _params = params;
    if (self.missue) {
        self.missue.params = params;
    }
}

// lazyInits
- (NSMutableDictionary<NSString *,SoulScope *> *)cellScopesDictionary {
    if(_cellScopesDictionary == nil) {
        _cellScopesDictionary = [[NSMutableDictionary<NSString *,SoulScope *> alloc] init];
    }
    return _cellScopesDictionary;
}





@end
