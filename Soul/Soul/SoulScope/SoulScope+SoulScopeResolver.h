//
//  SoulScope+SoulScopeResolver.h
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulScope.h"

@interface SoulScope (SoulScopeResolver)
-(instancetype)initScopeWithParams:(NSDictionary *)params;

- (void)soulBlockResolve:(SoulBlock)soulBlock;

@end
