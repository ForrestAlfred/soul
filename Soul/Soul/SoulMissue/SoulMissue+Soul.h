//
//  SoulMissue+Soul.h
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulMissue.h"
#import "SoulBlocks.h"

@interface SoulMissue (Soul)
- (void)init:(SoulInitBlock)soulInitBlock;

    // Missue
-(void)variables:(SoulBlock)soulBlock;
-(void)functions:(SoulBlock)soulBlock;
-(void)services:(SoulBlock)soulBlock;

@end
