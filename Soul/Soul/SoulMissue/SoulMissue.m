    //
    //  SoulTissue.m
    //  EvolutionForSoul
    //
    //  Created by 徐腾浩 on 4/1/16.
    //  Copyright © 2016 daopr. All rights reserved.
    //

#import "SoulMissue.h"
#import "SoulMissue+Soul.h"

#import <ReactiveCocoa.h>
@interface SoulMissue ()
    // Soul data outlets

@end

@implementation SoulMissue

    // Example
-(void)initSoul {
    
    @weakify(self);
    [self init:^(NSDictionary *params) {
        @strongify(self);
    }];
    
    [self variables:^{
        @strongify(self);
        
    }];
    [self functions:^{
        @strongify(self);
        
    }];
    [self services:^{
        @strongify(self);
        
    }];
    self.soulWillAppear = ^{
        @strongify(self);
    };
}

- (void)soulInit {
    
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Soul Warning"
                                   reason:@"SoulMissue Init ItSelf; Maybe You Passing Wrong Class; Maybe Your Missue Is Not Inherit From Soul"
                                 userInfo:nil];
    
}

- (instancetype)initMissueWithParams:(NSDictionary *)params {
    if (!(self = [super init]))   {return nil;}
    self.params = params;
    
    @weakify(params);
    self.soul = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(params);
        [subscriber sendNext:params];
        [subscriber sendCompleted];
        return nil;
    }];
    
    [self initSoul];
    [self soulInit];
    
    return self;
}

@end
