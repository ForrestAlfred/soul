    //
    //  SoulTissue.h
    //  EvolutionForSoul
    //
    //  Created by 徐腾浩 on 4/1/16.
    //  Copyright © 2016 daopr. All rights reserved.
    //
#import "SoulBlocks.h"

@class RACSignal;
@interface SoulMissue : NSObject

@property (nonatomic, strong) NSDictionary *params;

@property (nonatomic, strong) SoulBlock soulWillAppear;
@property (nonatomic, strong) SoulBlock soulWillDisappear;
@property (nonatomic, strong) SoulBlock soulDidLoad;

@property (nonatomic, strong) SoulBlock soulMemoryWarning;

@property (nonatomic, strong) RACSignal *soul;

- (void)initSoul;
- (void)soulInit;

- (instancetype)initSoulWithParams:(NSDictionary *)params soul:(Class)soulClass ;
- (instancetype)initMissueWithParams:(NSDictionary *)params;
@end
