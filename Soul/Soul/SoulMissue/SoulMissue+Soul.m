//
//  SoulMissue+Soul.m
//  EvolutionForSoul
//
//  Created by 徐腾浩 on 4/4/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "SoulMissue+Soul.h"

#import <ReactiveCocoa.h>
@implementation SoulMissue (Soul)
- (void)init:(SoulInitBlock)soulInitBlock {
    [self.soul subscribeNext:soulInitBlock];
}

-(void)variables:(SoulBlock)soulBlock {
	
}

-(void)functions:(SoulBlock)soulBlock {
	
}

-(void)services:(SoulBlock)soulBlock {
	
}

@end
