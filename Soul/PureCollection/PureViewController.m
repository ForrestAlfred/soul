//
//  PureViewController.m
//  Soul
//
//  Created by DaoPr on 4/7/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "PureViewController.h"
#import "CircleCollectionView.h"
#import <Masonry.h>

@interface PureViewController ()
@property (nonatomic, strong) CircleCollectionView  *collectionCircleView;
@property (nonatomic, strong) UIView *baseBackground;
@end

@implementation PureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
        //    self.collectionCircleView = [[CircleCollectionView alloc] initWithFrame:CGRectMake(0, 100, self.view.bounds.size.width, 200.0)];
    self.collectionCircleView = [[CircleCollectionView alloc] init];
    
        //    [self.view addSubview:self.collectionCircleView];
    
    self.collectionCircleView.dataArray = [NSMutableArray arrayWithObjects:@"", @"", @"", @"", @"", @"", nil];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.baseBackground = [UIView new];
    self.collectionCircleView.background = self.baseBackground;
    
    [self.view addSubview:self.baseBackground];
    [self.baseBackground addSubview:self.collectionCircleView];
    
    [self.baseBackground mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.viewIfLoaded);
    }];
        //
        //    [self.collectionCircleView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.edges.equalTo(self.baseBackground);
        //    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
