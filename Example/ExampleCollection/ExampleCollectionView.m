//  ExampleCollectionView.m
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//
#define SCREENWIDTH [ UIScreen mainScreen ].bounds.size.width
#define SCREENHEIGHT [ UIScreen mainScreen ].bounds.size.height
#import "ExampleCollectionView.h"
@interface ExampleCollectionView ()

@property (weak, nonatomic) IBOutlet UIImageView *enew;
@property (nonatomic, strong) NSIndexPath *lastIndexPath;
@end

@implementation ExampleCollectionView

-(void)soulInit {
    self.userInteractionEnabled = YES;
    self.soulWillAppear = ^{
      self.backgroundColor = [UIColor blueColor];
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.background);
            make.left.right.equalTo(self.background);
            make.height.equalTo(self.mas_width).multipliedBy((SCREENWIDTH)/(SCREENHEIGHT));
        }];
    };
    self.soulDidLoad = ^{
        
    
    };
    
    
    [self observe];
}

-(void)layoutSubviews {
    [super layoutSubviews];
   self.flowLayout.itemSize = self.frame.size;
}

-(void)observe {
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIApplicationWillChangeStatusBarOrientationNotification object:nil] subscribeNext:^(NSNotification * notification) {
        self.lastIndexPath = [[self indexPathsForVisibleItems] lastObject];
        NSNumber *orientationKey = notification.userInfo[@"UIApplicationStatusBarOrientationUserInfoKey"];
        self.flowLayout.itemSize = CGSizeMake(self.bounds.size.width, self.bounds.size.height);
            //            [self.controlsArray each:^(UIView *control) {
            //                control.hidden = orientationKey.intValue >2 ? YES:NO;
            //            }];
        [[RACScheduler mainThreadScheduler]afterDelay:0.0001 schedule:^{
            [self scrollToItemAtIndexPath:self.lastIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }];
    }];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}
@end
