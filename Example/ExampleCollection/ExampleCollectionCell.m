//
//  ExampleCollectionCell.m
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "ExampleCollectionCell.h"
#import "Masonry.h"
#import "ExampleCollectionCellScope.h"
@interface ExampleCollectionCell ()
@property (nonatomic, strong) ExampleCollectionCellScope *scope;
@end
@implementation ExampleCollectionCell
-(void)initSoul {
    @weakify(self);
    
    self.soulWillAppear = ^{
    @strongify(self);
        LxPrintAnything(soulWillAppear);
        LxDBAnyVar(self.displayLabel.text);
    };
    self.soulWillDisappear = ^{
    @strongify(self);
        LxPrintAnything(soulWillDisappear);
        LxDBAnyVar(self.displayLabel.text);
    };
    
}

@end
