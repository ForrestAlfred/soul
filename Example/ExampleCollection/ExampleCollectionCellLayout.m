//
//  ExampleCollectionCellLayout.m
//  Soul
//
//  Created by DaoPr on 4/11/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "ExampleCollectionCellLayout.h"

@implementation ExampleCollectionCellLayout
- (instancetype)init
{
    self = [super init];
    if (self) {
//        self.itemSize = CGSizeMake(320,240);
        self.minimumInteritemSpacing = 0.0;
        self.minimumLineSpacing = 0.0;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.sectionInset = UIEdgeInsetsZero;
    }
    return self;
}
@end
