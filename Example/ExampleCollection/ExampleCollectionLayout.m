//
//  ExampleCollectionLayout.m
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "ExampleCollectionLayout.h"
#import "LxDBAnything.h"

@implementation ExampleCollectionLayout
//-(void)prepareLayout {
//    LxDBAnyVar(self.collectionView.frame);
//        self.itemSize = self.collectionView.frame.size;
//}
- (instancetype)init
{
    self = [super init];
    if (self) {

        self.minimumInteritemSpacing = 0.0;
        self.minimumLineSpacing = 0.0;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.sectionInset = UIEdgeInsetsZero;
    }
    return self;
}
//
//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//    }
//    return self;
//}
////
////- (void)prepareLayout //这里做尺寸定义
////{
////    [super prepareLayout];
////        //将左右各切掉一部分（整个collectionView的宽度减去cell的宽度除以2），让cell居中显示
////    CGFloat inset = (self.collectionView.frame.size.width - self.itemSize.width) * 0.5;
////    self.sectionInset = UIEdgeInsetsMake(0, inset, 0, inset);
////}
////
////- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
////{
////    return YES;
////}
////
////- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
////{
////        //计算当前UICollectionView的可见区域的frame
////    CGRect visiableRect;
////        //当前可见区域的尺寸等于
////    visiableRect.size = self.collectionView.frame.size;
////    visiableRect.origin = self.collectionView.contentOffset;
////    
////        //1,计算当前屏幕水平中心的坐标
////    CGFloat centerX = self.collectionView.contentOffset.x + self.collectionView.frame.size.width * 0.5;
////    
////        //2,得到cell的布局属性数组
////    NSArray *array = [super layoutAttributesForElementsInRect:rect];
////        //3,遍历数组，得到每一个cell的布局属性，并根据每一个cell的水平坐标和当前屏幕的水平中心坐标的差值，计算cell的缩放倍数
////    for (UICollectionViewLayoutAttributes *attrs in array) {
////        
////            //如果这个cell不可见，就不重定义它的尺寸
////        if (!CGRectIntersectsRect(visiableRect, attrs.frame)) {
////            continue;
////        }
////        
////            //4,单个cell的水平x值
////        CGFloat itemCenterX = attrs.center.x;
////            //6,计算cell水平坐标和屏幕中心坐标的差值
////        CGFloat diff = ABS(itemCenterX - centerX);
////            //7,用差值除以(collectionView的宽度 - cell的宽度)，这个值是随便写的，只是为了得到一个小于1的值而已，因为上一步的到的尺寸差值很大，不能直接当比例算
////        CGFloat diff1 = diff / (self.collectionView.frame.size.width - self.itemSize.width);
////            //8,用1-diffs得到一个缩放比例
////        CGFloat diff2 = 1 - diff1;
////            //9,因为我想要一个放大的比例，所以比例要大于1，把上一步的值加1
////        CGFloat scale = 1 + self.amplificationConstant * diff2;
////            //10,更改attrs的transform属性，传入求得的缩放比例
////        attrs.transform = CGAffineTransformMakeScale(scale, scale);
////    }
////    return array;
////}
////
////    //滑动停止的时候调用，在这里矫正cell的位置，让最靠近中心的cell居中
////- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity
////{
////    LxDBAnyVar(self.collectionView.contentSize);
////    LxDBAnyVar(self.collectionView.frame);
////
////        //停止滑动的时刻下，UICollectionView内当前滚动的状态
////    CGRect lastRect;
////    lastRect.origin = proposedContentOffset;
////    lastRect.size = self.collectionView.frame.size;
////    
////        //当前屏幕中心点
////    CGFloat centerX = proposedContentOffset.x + self.collectionView.frame.size.width * 0.5;
////    
////        //获取停止滑动的时刻下的布局属性数组
////    NSArray *array = [self layoutAttributesForElementsInRect:lastRect];
////    
////        //定一个参数变量存储每一个cell中心点和当前屏幕中心点的差值
////    CGFloat adjustOffestX = MAXFLOAT;
////    
////    for (UICollectionViewLayoutAttributes  *attrs in array) {
////            //如果有一个cell的差值比上一个小，就让adjustOffestX的值为新的差值，遍历之后得到最小的差值
////        if (ABS(attrs.center.x - centerX) < ABS(adjustOffestX)) {
////            adjustOffestX = attrs.center.x - centerX;
////        }
////    }
////    
////        //根据计算的差值，矫正最后应该停止的位置
////    return CGPointMake(proposedContentOffset.x + adjustOffestX, proposedContentOffset.y);
////
////}
//
//
//    //Asks the delegate for the margins to apply to content in the specified section.安排初始位置
//    //使前后项都能居中显示
//
//    //Asks the delegate for the margins to apply to content in the specified section.安排初始位置
//    //使前后项都能居中显示
//
//
//    //scroll 停止对中间位置进行偏移量校正
//- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity {
//    CGFloat offsetAdjustment = MAXFLOAT;
//        ////  |-------[-------]-------|
//        ////  |滑动偏移|可视区域 |剩余区域|
//        //是整个collectionView在滑动偏移后的当前可见区域的中点
//    CGFloat centerX = proposedContentOffset.x + (CGRectGetWidth(self.collectionView.bounds) / 2.0);
//        //    CGFloat centerX = self.collectionView.center.x; //这个中点始终是屏幕中点
//        //所以这里对collectionView的具体尺寸不太理解，输出的是屏幕大小，但实际上宽度肯定超出屏幕的
//    
//    CGRect targetRect = CGRectMake(proposedContentOffset.x, 0.0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
//    
//    NSArray *array = [super layoutAttributesForElementsInRect:targetRect];
//    
//    for (UICollectionViewLayoutAttributes *layoutAttr in array) {
//        CGFloat itemCenterX = layoutAttr.center.x;
//        
//        if (ABS(itemCenterX - centerX) < ABS(offsetAdjustment)) { // 找出最小的offset 也就是最中间的item 偏移量
//            offsetAdjustment = itemCenterX - centerX;
//        }
//    }
//    
//    return CGPointMake(proposedContentOffset.x + offsetAdjustment, proposedContentOffset.y);
//}
//
//
//static CGFloat const ActiveDistance = 80;
//static CGFloat const ScaleFactor = 0.2;
//    //这里设置放大范围
//- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
//{
//    
//    NSArray *array = [super layoutAttributesForElementsInRect:rect];
//    
//    CGRect visibleRect = (CGRect){self.collectionView.contentOffset, self.collectionView.bounds.size};
//    
//    for (UICollectionViewLayoutAttributes *attributes in array) {
//            //如果cell在屏幕上则进行缩放
//        if (CGRectIntersectsRect(attributes.frame, rect)) {
//            
//            attributes.alpha = 0.5;
//            
//            CGFloat distance = CGRectGetMidX(visibleRect) - attributes.center.x;//距离中点的距离
//            CGFloat normalizedDistance = distance / ActiveDistance;
//            
//            if (ABS(distance) < ActiveDistance) {
//                CGFloat zoom = 1 + ScaleFactor * (1 - ABS(normalizedDistance)); //放大渐变
//                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0);
//                attributes.zIndex = 1;
//                attributes.alpha = 1.0;
//            }
//        }
//    }
//    
//    return array;
//}
@end
