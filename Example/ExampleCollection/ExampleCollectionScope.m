//
//  ExampleCollectionScope.m
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define YYMaxSections 100.0
#import "ExampleCollectionScope.h"
#import "ExampleCollectionLayout.h"
#import "ExampleCollectionCell.h"
#import "ObjectiveSugar.h"
#import "MKCollectionViewCircleCell.h"
#import "CircleCollectionView.h"
#import "Masonry.h"
#import "ReactiveCocoa.h"
#import "SoulCollectionView.h"
#import "SoulCollectionViewCell.h"

@interface ExampleCollectionScope ()
@property (nonatomic, strong) CircleCollectionView  *collectionCircleView;
@property (nonatomic, weak) UICollectionView *collectionView;

@end

@implementation ExampleCollectionScope
-(void)soulInit {

}

- (void)initSoul {
    @weakify(self);
    
    self.soulWillAppear  = ^{
        @strongify(self);
        
//        self.collectionCircleView = [[CircleCollectionView alloc] init];
//     
//        [self.background addSubview:self.collectionCircleView];
//        
//        self.collectionCircleView.contentView = self.background;

    self.collectionView.pagingEnabled = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;

    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ExampleCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([ExampleCollectionCell class])];
    
//    [self.background addSubview:self.collectionView];
    };
}

@end
