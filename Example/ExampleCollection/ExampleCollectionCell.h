//
//  ExampleCollectionCell.h
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Soul.h"

@interface ExampleCollectionCell :SoulCollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *displayImageView;
@property (weak, nonatomic) IBOutlet UILabel *displayLabel;

@end
