//
//  CircleCollectionView.m
//  PureCollectionView
//
//  Created by DaoPr on 4/7/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "CircleCollectionView.h"
#import "MKCollectionViewCircleCell.h"
#import "Masonry.h"
#import "ReactiveCocoa.h"
#import "SoulCollectionView.h"

@interface CircleCollectionView ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) SoulCollectionView *collectionView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) NSIndexPath *lastIndexPath;
@property (nonatomic, strong) NSTimer *scheduledTimer;
@end
@implementation CircleCollectionView

#define YYMaxSections 100.0
-(void)initSoul {
    self.dataArray = [NSMutableArray arrayWithObjects:@"", @"", @"", @"", @"", @"", nil];
   
}
- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

//- (instancetype)initWithFrame:(CGRect)frame {
//    self = [super initWithFrame:frame];
//    if (self) {
//        [self setupSubviews];
//    }
//    return self;
//}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    [self setupCollectionView];
    [self setupPageControl];
    [self observe];
//    [self addTimer];
}

- (void)setupCollectionView {
    self.collectionView = [[SoulCollectionView alloc] initWithFrame:CGRectMake(0, 0, 320, 240) collectionViewLayout:[self collectionViewFlowLayout]];
    
    self.frame = CGRectMake(0, 0, 320, 240);
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MKCollectionViewCircleCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([MKCollectionViewCircleCell class])];
    
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

- (UICollectionViewFlowLayout *)collectionViewFlowLayout {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(320,240);
    flowLayout.minimumInteritemSpacing = 0.0;
    flowLayout.minimumLineSpacing = 0.0;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.sectionInset = UIEdgeInsetsZero;
    return flowLayout;
}

- (void)setupPageControl {
    if (_pageControl) {
        return;
    }
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 20.0)];
    _pageControl.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height - 20.0);
    _pageControl.numberOfPages = _dataArray.count;
    _pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    _pageControl.currentPageIndicatorTintColor = [UIColor orangeColor];
    _pageControl.backgroundColor = [UIColor redColor];
//    [self addSubview:_pageControl];
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    if (self.collectionView) {
        _pageControl.numberOfPages = _dataArray.count;
        [self.collectionView reloadData];
    }
}

#pragma mark - colletionView dataSource && delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return YYMaxSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MKCollectionViewCircleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MKCollectionViewCircleCell class]) forIndexPath:indexPath];
    NSLog(@"%@", indexPath);
    ;
    NSLog(@"%@",[self.collectionView visibleCells]);
    cell.displayImageView.backgroundColor = [UIColor cyanColor];
//    switch (indexPath.row) {
//        case 0:
//        {
//            cell.displayImageView.backgroundColor = [UIColor orangeColor];
//        }
//        case 1:
//        {
//            cell.displayImageView.backgroundColor = [UIColor cyanColor];
//        }
//            break;
//        case 2:
//        {
//            cell.displayImageView.backgroundColor = [UIColor brownColor];
//        }
//            break;
//        default:
//            break;
//    }
    cell.displayLabel.text = [NSString stringWithFormat:@"%ld", indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//
//}

#pragma mark - scrollView delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    [self removeTimer];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    [self addTimer];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int page = (int)(scrollView.contentOffset.x/scrollView.frame.size.width+0.5)%self.dataArray.count;
    self.pageControl.currentPage = page;
    NSLog(@"%@",[self.collectionView visibleCells]);
    NSLog(@"%@",[self.collectionView visibleCells]);
    
}

#pragma mark - nextPage
- (void)nextPage:(id)sender {
        // 1. 得到当前正在显示的cell的indexPath，（只有一个）
    NSIndexPath *currentIndexPath = [[self.collectionView indexPathsForVisibleItems] lastObject];
    
        // 2. 得到YYMaxSections/2对应的section的indexPath，显示此indexPath对应的cell
    NSIndexPath *currentIndexPathReset = [NSIndexPath indexPathForItem:currentIndexPath.item inSection:YYMaxSections/2];
    [self.collectionView scrollToItemAtIndexPath:currentIndexPathReset atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    
        // 3. 如果当前section的row未显示完全，则row+1，否则section+1，row置为0
    NSInteger nextItem = currentIndexPathReset.item + 1;
    NSInteger nextSection = currentIndexPathReset.section;
    if (nextItem == self.dataArray.count) {
        nextItem = 0;
        nextSection++;
    }
    
        // 4. 位移显示效果
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:nextSection];
    [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

-(void)observe {
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIApplicationWillChangeStatusBarOrientationNotification object:nil] subscribeNext:^(NSNotification * notification) {
        self.lastIndexPath = [[self.collectionView indexPathsForVisibleItems] lastObject];
        
        NSNumber *orientationKey = notification.userInfo[@"UIApplicationStatusBarOrientationUserInfoKey"];
            //            [self.collectionView reloadData];
        
        if (orientationKey.intValue>2) {
            self.frame = CGRectMake(0, 0, 568, 320);

            self.collectionView.frame = CGRectMake(0, 0, 568, 320);
        } else {
            self.frame = CGRectMake(0, 0, 320, 240);

            self.collectionView.frame = CGRectMake(0, 0, 320, 240);
        }
        
    UICollectionViewFlowLayout *flowLayout =
        self.collectionView.collectionViewLayout;
        flowLayout.itemSize = CGSizeMake(self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
            //            [self.controlsArray each:^(UIView *control) {
            //                control.hidden = orientationKey.intValue >2 ? YES:NO;
            //            }];
            //           [self.collectionView scrollToItemAtIndexPath:self.lastIndexPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
 
        [[RACScheduler mainThreadScheduler]afterDelay:0.001 schedule:^{
                  [self.collectionView scrollToItemAtIndexPath:self.lastIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }];
    }];
}

@end
