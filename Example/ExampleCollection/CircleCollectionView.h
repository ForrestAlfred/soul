//
//  CircleCollectionView.h
//  PureCollectionView
//
//  Created by DaoPr on 4/7/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoulView.h"

@interface CircleCollectionView : SoulView

@property (nonatomic, strong) NSMutableArray *dataArray;

@end
