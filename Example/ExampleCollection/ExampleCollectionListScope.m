//
//  ExampleCollectionListScope.m
//  Soul
//
//  Created by DaoPr on 4/11/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "ExampleCollectionListScope.h"
#import "ExampleCollectionCell.h"
#define YYMaxSections 600.0
@interface ExampleCollectionListScope ()
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, weak) ExampleCollectionCell *lastCell;
@end

@implementation ExampleCollectionListScope
-(void)initSoul {
    self.dataArray = [NSMutableArray arrayWithObjects:@"", @"", @"", @"", @"", @"", nil];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:300];
    self.soulWillAppear = ^{
    };
    self.soulDidLoad = ^{
        [[RACScheduler mainThreadScheduler]afterDelay:0.0001 schedule:^{
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:50] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        }];
    };

}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return YYMaxSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ExampleCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ExampleCollectionCell class]) forIndexPath:indexPath];
//    NSLog(@"%@", indexPath);
//    ;
//    NSLog(@"%@",[self.collectionView visibleCells]);
    cell.displayImageView.backgroundColor = [UIColor cyanColor];
    
    cell.displayLabel.text = [NSString stringWithFormat:@"%ld", indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


#pragma mark - scrollView delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
        //    [self removeTimer];
//    LxDBAnyVar(self.collectionView);

     LxPrintAnything(begin);
    NSLog(@"%@",[self.collectionView visibleCells]);
    ExampleCollectionCell* cell = [self cellIfWillDisappear];
    if (cell.soulWillDisappear) {
        cell.soulWillDisappear();
    }
    self.lastCell = cell;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    LxPrintAnything(end);
    NSLog(@"%@",[self.collectionView visibleCells]);
    ExampleCollectionCell* cell = [self cellIfWillAppear];
    if (cell.soulWillAppear) {
        cell.soulWillAppear();
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
        //    [self addTimer];
}

- (UICollectionViewCell *)cellIfWillAppear {
    ExampleCollectionCell* cell;
    if ([self.collectionView visibleCells].count == 2) {
        for (SoulCollectionViewCell *soulCell in [self.collectionView visibleCells]) {
            if (soulCell.frame.origin.x != self.lastCell.frame.origin.x) {
                return soulCell;
            }
        }
    }
    return cell;
}
- (UICollectionViewCell *)cellIfWillDisappear {
    if ([self.collectionView visibleCells].firstObject.frame.origin.x == 0.0) {
        return nil;
    }
    return [self.collectionView visibleCells].firstObject;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    LxPrintAnything(did);
//    NSLog(@"%@",[self.collectionView visibleCells]);
}


@end
