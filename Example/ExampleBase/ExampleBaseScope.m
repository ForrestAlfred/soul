//
//  ExampleBaseScope.m
//  Soul
//
//  Created by DaoPr on 4/5/16.
//  Copyright © 2016 daopr. All rights reserved.
//

#import "ExampleBaseScope.h"
#import "ExampleCollectionScope.h"
#import "SoulScope.h"

@implementation ExampleBaseScope
- (void)soulInit {
    [self init:^(NSDictionary *params) {
//        SoulScope* scope =  [[ExampleCollectionScope alloc]initScopeWithParams:params];
//          self.subScopes = [@[scope]mutableCopy];
self.subScopeClasses = @[[ExampleCollectionScope class]];
    }];
}
@end
